(** Une interface fonctionnelle à l'outil Zenity
    Dernière version ici: http://besson.qc.to/publis/Zenity/
    @author Lilian Besson <lilian.besson[AT]ens-cachan.fr>
*)

open Pervasives;;

let print_string s =
  print_string s;
  flush_all ();;

(** {6 Ouvertures des modules de bases} *)


(** Module Sys. *)
open Sys;;

(*----------------------------------------------------------------------------------------------------------------------------------------------*)

(** Fonction ecrire_sortie : plus pratique que output. *)
let ecrire_sortie monoutchanel machaine =
  output monoutchanel machaine 0 (String.length machaine);
  flush monoutchanel;;

(** Fonction ecrire_dans_fichier : pour écrire la chaine dans le fichier à l'adresse renseignée. *)
let ecrire_dans_fichier ~chaine ~adresse =
  let mon_out_channel = open_out adresse in
  ecrire_sortie mon_out_channel chaine;;

(** Fonction in_list : tail recursive plus pratique que List.exists (fun x -> x=a) l. *)
let rec in_list element = function
  |[] -> false;
  |el :: ssl -> if (element = el) then true else (in_list element ssl);;

(** Pour extraire une sous-string, plus pratique que String.sub. *)
let monsub mastr idebut taille =
  let res = ref "" in
  if taille >= 0 then res := (String.sub mastr idebut taille);
  !res;;

(** Decoupe_custom ~e:<caractere_separateur> : découpe la chaine d'entree en morceau, en commancant un nouveau morceau pour chaque "e" trouvé. *)
let rec decoupe_custom ?(e = ['#']) ma_string =
  let n = (String.length ma_string);
  and result = ref [];
  and i = ref 0; in
  if (n > 0) then
    begin
      while ( (!i < (n - 1)) && not(in_list ma_string.[!i]  e)) do
	incr i;
      done;
      if (!i = (n - 1))  then result := [ma_string];
      if (!i <= (n - 2)) then result := [(monsub ma_string 0 !i)]@(decoupe_custom ~e (monsub ma_string (!i + 1) (n - !i - 1) ));
    end;
  !result;;

(** {5 Différentes fonctions parse} *)

(** Renvoie le contenu de la première ligne d'un fichier texte. *)
let parse_file ~file =	(* fichiers a une seule lignes *)
  input_line (open_in file);;

(** Renvoie le contenu du fichier texte en entier. *)
let parse_file_full ~file =
  (*(** file : correspond à l'adresse (relative ou absolue) du
    fichier que l'on veut lire. *) (** /!\ CONVENTION : on utilise
    toujours la réprésentation fichier de Unix :
    /home/superman/Bureau/puzzle_de_la_mort_qui_tue.instance *) *)
  (** On ouvre le canal. *)
  let canal_entree  = open_in file; in
  let finlecture = ref false; in
  let nb_ligne = ref 0; in
  let result = ref "" in
  let ( ^= ) s a = s := !s ^ a; in

  (** On commence à lire, *)
  let rec auxi mon_ce =
    let res_input = (try (((input_line mon_ce))) with End_of_file -> (finlecture := true; "")); in
    (** Si on a pas finit de lire, on analyse : *)
    if (not !finlecture)
    then
      (
	result ^= res_input;
	(** On a encore des lignes à lire (on écrit donc un caractère nouvelle ligne)  *)
	result ^= "\n";
	incr nb_ligne;

	(auxi mon_ce);
      )
    else close_in canal_entree;
  (** Sinon, on clos le canal d'entrée, et on passe a la suite. *)
  in

  auxi canal_entree;
  (** On conclut en fermant le canal d'entréé et en renvoyant le contenu. *)
  close_in canal_entree;
  !result;;

(** {6 Module MOCamlStd : quelques fonctions de lecture de fichiers }*)

type 'a _mut_list = {
  hd : 'a;
  mutable tl : 'a _mut_list;
}

let input_list ch =
  let _empty = Obj.magic [] in
  let rec loop dst =
    let r = { hd = input_line ch; tl = _empty } in
    dst.tl <- r;
    loop r in
  let r = { hd = Obj.magic(); tl = _empty } in
  try loop r
  with
      End_of_file ->
	Obj.magic r.tl
(** Returns the list of lines read from an input channel. *)

let buf_len = 8192

let input_all ic =
  let rec loop acc total buf ofs =
    let n = input ic buf ofs (buf_len - ofs) in
    if n = 0 then
      let res = String.create total in
      let pos = total - ofs in
      let _ = String.blit buf 0 res pos ofs in
      let coll pos buf =
        let new_pos = pos - buf_len in
        String.blit buf 0 res new_pos buf_len;
        new_pos in
      let _ = List.fold_left coll pos acc in
      res
    else
      let new_ofs = ofs + n in
      let new_total = total + n in
      if new_ofs = buf_len then
        loop (buf :: acc) new_total (String.create buf_len) 0
      else loop acc new_total buf new_ofs in
  loop [] 0 (String.create buf_len) 0
(** Returns the list of lines read from an input channel. *)

let input_file ?(bin=false) fname =
  let ch = (if bin then open_in_bin else open_in) fname in
  let str = input_all ch in
  close_in ch;
  str
(** returns the data of a given filename. *)

(** Affiche une liste de fichiers. *)
let cat f = List.iter (fun s -> print_string (parse_file_full ~file:s)) f;;

(** Pour récupérer les coordonnées (r,g,b) d'une couleur du module Graphics. *)
let from_rgb (c : Graphics.color)  =
  let r = c / 65536  and  g = c / 256 mod 256  and  b = c mod 256
  in (r,g,b) ;;


(** Transforme un char en string. *)
let string_of_char c = String.make 1 c;;
(* function | ii when ((ii >= 0) && (ii <= 9)) *)

let char_from_int ii = Pervasives.char_of_int (ii + (Pervasives.int_of_char '0'));;
let char_of_hex = function
  | 10 -> 'A' | 11 -> 'B' | 12 -> 'C' | 13 -> 'D' | 14 -> 'E' | 15 -> 'F'
  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 as ii	->	char_from_int ii
  | _ -> (failwith ("hex_of_int"));;

let jj i = (string_of_char (char_of_hex (i / 16))) ;;
let j i =  (string_of_char (char_of_hex (i mod 16))) ;;

(** Transforme un entier entre 0 et 255 en sa représentation hexa dans
    une chaine. *)
let hex_of_int i =
  (jj i)^""^(j i);;
(** [On a affaire a un bug absurde de Ocaml : si je fait (jj i)^(j i),
    cela ne renvoit pas ce qu'il faut !!!] *)
(*iterhex hex_of_int;;*)

let hex_of_color c =
  let (r,g,b) = from_rgb c in
  (hex_of_int r)^(hex_of_int g)^(hex_of_int b);;

(*print_string (hex_of_color Graphics.green);*)
(*print_string (hex_of_color Graphics.red);*)
(*print_string (hex_of_color Graphics.black);*)
(*print_string (hex_of_color Graphics.blue);;*)


(** Transforme un char en héxa en sa valeur entière. Accepte aussi
    bien les minuscules que les majuscules. *)
let int_of_charhex c = match c with
  | '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' -> int_of_string (string_of_char c);
  | 'A' | 'a' -> 10;
  | 'B' | 'b' -> 11;
  | 'C' | 'c' -> 12;
  | 'D' | 'd' -> 13;
  | 'E' | 'e' -> 14;
  | 'F' | 'f' -> 15;
  | _ -> (failwith ("int_of_charhex"));;

(** Transforme un caractère bi hexa (chaine de deux caractères entre 0
    et A) en sa valeur. *)
let int_of_hex2 s =
  let (x,y) = (int_of_charhex s.[0], int_of_charhex s.[1]) in
  x * 16 + y;;

(** Petite fonction outil pour récupérer le code RGB utilisé par
    Graphics, à partir du rendu utilisé par Zenity dans sa fonction
    "--color-selection".  *)
let parse_color ~file =
  let s = (parse_file ~file:file) in
  let (s1, s2, s3) = ( (monsub s 1 2), (monsub s 5 2), (monsub s 9 2)) in
  Graphics.rgb (int_of_hex2 s1) (int_of_hex2 s2) (int_of_hex2 s3);;

(** Petite fonction outil pour extraire un entier du fichier texte se
    trouvant à l'adresse "file". Cela suppose bien sur que le dit fichier
    ne contiennent qu'un entier. *)
let parse_int ~file = int_of_string (parse_file ~file:file) ;;


(*----------------------------------------------------------------------------------------------------------------------------------------------*)

(** L'exception qui est renvoyé si le processus appelé par la fonction
    faireZ retourne un code d'erreur UNIX spécifiant une erreur. *)
exception ZenityOcaml_Erreur_Annule of int;;

(** Exécute une commande système, mais lève une exception en cas de
    problème du processus appelé. *)
let faireZ cmd =
  match (Sys.command cmd) with
    | 0	-> ();
    | n 	-> raise (ZenityOcaml_Erreur_Annule(n));;

(** {6 Fonctions pour traiter les arguments} *)

let ios = int_of_string and soi = string_of_int;;
let bool_of_string = function | "true" -> true | _ -> false
and string_of_bool = function | true -> "true" | false -> "";;


(** {7 Les Arguments toujours présents}*)

let title_ = function
  | ""	-> ""
  | t	-> (" --title=\""^t^"\" ");;
(** Traite un titre. *)

let window_icon_ = function
  | ""	-> ""
  | t	-> (" --window-icon=\""^t^"\" ");;
(** Traite une icone de fenêtre. *)

let width_ = function
  | 0	-> ""
  | x	-> (" --width="^(string_of_int x)^" ");;
(** Traite une largeur de fenêtre. *)

let height_ = function
  | 0	-> ""
  | x	-> (" --height="^(string_of_int x)^" ");;
(** Traite une hauteur de fenêtre. *)

let expiration_ = function
  | 0	-> ""
  | x	-> (" --timeout="^(string_of_int x)^" ");;
(** Traite un temps d'expiration. *)

let gestion_arg ~nom_arg ~val_arg ~def_arg =
  if (val_arg = def_arg)
  then ""
  else (" --"^nom_arg^" ");;
(** Macro pour traiter un argument de nom [nom_arg], de valeur appelée
    [val_arg] et de valeur par défaut [def_arg] (polymorphiques). *)

type add_forms = Add_entry of string | Add_password of string | Add_calendar of string | NoAdd;;
(** Type des entrées supplémentaires de la fonction forms (formulaire). *)

let print_add_forms = function
  | Add_calendar(s)	-> ("--add-calendar="^s^" ")
  | Add_password(s)	-> ("--add-password="^s^" ")
  | Add_entry(s)		-> ("--add-entry="^s^" ")
  | NoAdd			-> ("");;
(** Une méthode simple pour afficher des éléments de ce type. *)

(** {7 Les arguments booléens} *)
let hide_text_ b	=	gestion_arg ~nom_arg:"hide-text"  ~val_arg:b ~def_arg:false
and hide_header_ b	=	gestion_arg ~nom_arg:"hide-header"  ~val_arg:b ~def_arg:false
and print_partial_ b	=	gestion_arg ~nom_arg:"print-partial"  ~val_arg:b ~def_arg:false
and hide_value_ b	=	gestion_arg ~nom_arg:"hide-value"  ~val_arg:b ~def_arg:false
and pulsate_ 	b	=	gestion_arg ~nom_arg:"pulsate"  ~val_arg:b ~def_arg:false
and auto_close_ b	=	gestion_arg ~nom_arg:"auto-close"  ~val_arg:b ~def_arg:false
and auto_kill_ 	b	=	gestion_arg ~nom_arg:"auto-kill"  ~val_arg:b ~def_arg:false
and multiple_ 	b	=	gestion_arg ~nom_arg:"multiple"  ~val_arg:b ~def_arg:false
and save_ 	b	=	gestion_arg ~nom_arg:"save"  ~val_arg:b ~def_arg:false
and html_ 	b	=	gestion_arg ~nom_arg:"html"  ~val_arg:b ~def_arg:false
and directory_	s	=	gestion_arg ~nom_arg:"directory" ~val_arg:s ~def_arg:false
and confirm_overwrite_ b	=	gestion_arg ~nom_arg:"confirm-overwrite"  ~val_arg:b ~def_arg:false
and checklist_ 	b	=	gestion_arg ~nom_arg:"checklist"  ~val_arg:b ~def_arg:false
and radiolist_ 	b	=	gestion_arg ~nom_arg:"radiolist"  ~val_arg:b ~def_arg:false
and editable_ 	b	=	gestion_arg ~nom_arg:"editable"  ~val_arg:b ~def_arg:false
and no_wrap_ 	b	=	gestion_arg ~nom_arg:"no-wrap"  ~val_arg:b ~def_arg:false
and listen_ 	b	=	gestion_arg ~nom_arg:"listen"  ~val_arg:b ~def_arg:false
;;
(** {7 Les arguments entiers} *)
let value_ 	i	=	gestion_arg ~nom_arg:("value="^(string_of_int i))  ~val_arg:i ~def_arg:0
and min_value_ 	i	=	gestion_arg ~nom_arg:("min-value="^(string_of_int i))  ~val_arg:i ~def_arg:0
and max_value_ 	i	=	gestion_arg ~nom_arg:("max-value="^(string_of_int i))  ~val_arg:i ~def_arg:0
and step_ 	i	=	gestion_arg ~nom_arg:("step="^(string_of_int i))  ~val_arg:i ~def_arg:0
and percentage_ 	i	=	gestion_arg ~nom_arg:("percentage="^(string_of_int i))  ~val_arg:i ~def_arg:0
and colum_ 	i	=	gestion_arg ~nom_arg:("colum="^(string_of_int i))  ~val_arg:i ~def_arg:0
and hide_column_ 	i	=	gestion_arg ~nom_arg:("hide_column="^(string_of_int i))  ~val_arg:i ~def_arg:0
and print_column_ 	i	=	gestion_arg ~nom_arg:("print_column="^(string_of_int i))  ~val_arg:i ~def_arg:0
and display_ 	i	=	gestion_arg ~nom_arg:("display="^(string_of_int i))  ~val_arg:i ~def_arg:0
;;
(** {7 Les arguments strings} *)
let text_	s	=	gestion_arg ~nom_arg:("text=\""^s^"\"") ~val_arg:s ~def_arg:""
and entry_text_	s	=	gestion_arg ~nom_arg:("entry-text=\""^s^"\"") ~val_arg:s ~def_arg:""
and username_	s	=	gestion_arg ~nom_arg:("username=\""^s^"\"") ~val_arg:s ~def_arg:""
and url_	s	=	gestion_arg ~nom_arg:("url=\""^s^"\"") ~val_arg:s ~def_arg:""
and checkbox_	s	=	gestion_arg ~nom_arg:("checkbox=\""^s^"\"") ~val_arg:s ~def_arg:""
and font_	s	=	gestion_arg ~nom_arg:("font=\""^s^"\"") ~val_arg:s ~def_arg:""
and separator_	s	=	gestion_arg ~nom_arg:("separator=\""^s^"\"") ~val_arg:s ~def_arg:""
and filename_	s	=	gestion_arg ~nom_arg:("filename=\""^s^"\"") ~val_arg:s ~def_arg:""
and column_	s	=	gestion_arg ~nom_arg:("column=\""^s^"\"") ~val_arg:s ~def_arg:""
and file_filter_	s	=	gestion_arg ~nom_arg:("file-filter=\""^s^"\"") ~val_arg:s ~def_arg:""
and ok_label_	s	=	gestion_arg ~nom_arg:("ok-label=\""^s^"\"") ~val_arg:s ~def_arg:""
and cancel_label_	s	=	gestion_arg ~nom_arg:("cancel-label=\""^s^"\"") ~val_arg:s ~def_arg:""
;;
(** {7 Les arguments autres} *)
let day_  	i	=	gestion_arg ~nom_arg:("day="^(string_of_int i))  ~val_arg:i ~def_arg:0
and month_  	i	=	gestion_arg ~nom_arg:("month="^(string_of_int i))  ~val_arg:i ~def_arg:0
and year_  	i	=	gestion_arg ~nom_arg:("year="^(string_of_int i))  ~val_arg:i ~def_arg:0
and date_format_	s	=	gestion_arg ~nom_arg:("date-format=\""^s^"\"") ~val_arg:s ~def_arg:""
and forms_date_format_	s	=	gestion_arg ~nom_arg:("forms-date-format=\""^s^"\"") ~val_arg:s ~def_arg:""
and add_	= function
  | []	-> ""
  | spe	-> String.concat "" (List.map print_add_forms spe)
and color_	cgraphics	=	gestion_arg ~nom_arg:("color="^(hex_of_color cgraphics)) ~val_arg:cgraphics ~def_arg:Graphics.black
;;

(** {6 Autres} *)

let nom_fichier_temporaire = "/tmp/zenityOcaml.tmp" ;;
(** Nom du fichier temporaire ou est stocké le résultat de l'appel à
    Zenity. *)

let __debug__ = ref false;;
let char_of_string s = s.[0];;

let fairezenity s =
  if (!__debug__) then print_string ("APPEL A :\nzenity  --"^s^"  >  "^nom_fichier_temporaire);
  faireZ ("zenity  --"^s^"  >  "^nom_fichier_temporaire) ;;
(** Appel zenity avec une commande, et fork le résultat dans
    nom_fichier_temporaire*)

(** {6 Les différentes fonctions parse : pour récupérer le résultat de
    Zenity, écrit par un fork dans un fichier temporaire
    'nom_fichier_temporaire'} *)

let parse_calendar ~file = parse_file ~file:file
and parse_entry ~file = parse_file ~file:file
and parse_error ~file = parse_file ~file:file
and parse_info ~file = parse_file ~file:file
and parse_file_selection ~separator ~file =
  decoupe_custom ~e:[char_of_string separator] (parse_file ~file:file)
and parse_list ~separator ~file =
  let s = input_file ~bin:false file
  in
  if (0 < String.length s)
  then decoupe_custom ~e:[char_of_string separator] (monsub s 0 ((-1) + String.length s))
  else [""]
(*[parse_file ~file:file]*)
and parse_notification ~file = parse_int ~file:file
and parse_progress ~file = parse_int ~file:file
and parse_question ~file = bool_of_string (parse_file ~file:file)
and parse_warning ~file = parse_int ~file:file
and parse_scale ~file = parse_int ~file:file
and parse_text_info ~file = input_file ~bin:false file
and parse_color_selection ~file = parse_color ~file:file (* Graphics.green *)
and parse_password ~file = parse_file ~file:file
and parse_forms ~file = parse_file_full ~file:file
;;


(** {6 Définition des différentes fonctions utiles du module ZenityOcaml }) *)

(** {7 zenity --calendar} *)
let calendar
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(day = 0) ?(month = 0) ?(year = 0) ?(date_format = "")
    () =
  fairezenity ("calendar "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(day_ day)^(month_ month)^(year_ year)^(date_format_ date_format)^
		  " ");
  parse_calendar ~file:nom_fichier_temporaire;;

(** {7 zenity --entry} *)
let entry
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(entry_text = "") ?(hide_text = false)
    () =
  fairezenity ("entry "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(entry_text_ entry_text)^(hide_text_ hide_text)^
		  " ");
  parse_entry ~file:nom_fichier_temporaire;;

(** {7 zenity --password} *)
let password
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(username = "")
    () =
  fairezenity ("password "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (username_ username)^
		  " ");
  parse_password ~file:nom_fichier_temporaire;;

(** {7 zenity --forms} *)
let forms
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(separator = "") ?(forms_date_format = "") ?(add = [])
    () =
  fairezenity ("forms "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(separator_ separator)^(forms_date_format_ forms_date_format)^(add_ add)^
		  " ");
  parse_forms ~file:nom_fichier_temporaire;;

(** {7 zenity --color-selection} *)
let color_selection
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(color = Graphics.green)
    () =
  fairezenity ("color-selection "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (color_ color)^
		  " ");
  parse_color_selection ~file:nom_fichier_temporaire;;

(** {7 zenity --scale} *)
let scale
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0)
    ?(text = "") ?(value = 0) ?(min_value = 0) ?(max_value = 0) ?(step = 0) ?(print_partial = false) ?(hide_value = false)
    () =
  fairezenity ("scale "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^
		  (text_ text)^(value_ value)^(min_value_ min_value)^(max_value_ max_value)^(step_ step)^(print_partial_ print_partial)^(hide_value_ hide_value)^
		  " ");
  parse_scale ~file:nom_fichier_temporaire;;

(** {7 zenity --progress} *)
let progress
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(percentage = 0) ?(pulsate = false) ?(auto_close = false) ?(auto_kill = false)
    () =
  fairezenity ("progress "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(percentage_ percentage)^(pulsate_ pulsate)^(auto_close_ auto_close)^(auto_kill_ auto_kill)^
		  " ");;
(** N'est pas encore au point. *)
(*	parse_progress ~file:nom_fichier_temporaire;;*)

(** {7 zenity --file-selection} *)
let file_selection
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(filename = "") ?(multiple = false) ?(directory = false) ?(save = false) ?(separator = "#") ?(confirm_overwrite = false) ?(file_filter = "")
    () =
  fairezenity ("file-selection "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (filename_ filename)^(multiple_ multiple)^(directory_ directory)^(save_ save)^(separator_ separator)^(confirm_overwrite_ confirm_overwrite)^(file_filter_ file_filter)^
		  " ");
  parse_file_selection ~separator:separator ~file:nom_fichier_temporaire;;

(** {7 zenity --list} *)
let list
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(column = "Colonne") ?(checklist = false) ?(radiolist = false) ?(separator = ";") ?(multiple = true) ?(editable = false) ?(print_column = 0) ?(hide_column = 0) ?(hide_header = false) ?(liste = ["Entrée non renseignée"])
    () =
  fairezenity ("list "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(column_ column)^(checklist_ checklist)^(radiolist_ radiolist)^(separator_ separator)^(multiple_ multiple)^(editable_ editable)^(print_column_ print_column)^(hide_column_ hide_column)^(hide_header_ hide_header)^
		  (String.concat "" (List.map (fun s -> ("\""^s^"\" ")) liste ))^
		  " ");
  parse_list ~separator:separator ~file:nom_fichier_temporaire;;

(** {7 zenity --text-info} *)
let text_info
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(filename = "") ?(editable = false) ?(display = 0) ?(html = false) ?(url = "") ?(checkbox = "") ?(font = "")
    () =
  fairezenity ("text-info "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (filename_ filename)^(editable_ editable)^(display_ display)^(html_ html)^(url_ url)^(checkbox_ checkbox)^(font_ font)^
		  " ");
  parse_text_info ~file:nom_fichier_temporaire;;

(** {7 Un essai pour un éditeur de fichier très très très léger avec ces quelques fonctions.} *)
let edit filename
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0) ?(display = 0) ?(font = "")
    () =
  let contenu = (text_info ~checkbox:"Édition terminée ?" ~filename:filename ~editable:true ~title:(title^" Édition du fichier ["^filename^"]") ()) in
  let adresse = (entry ~title:(title^" Édition du fichier ["^filename^"]") ~text:("Ou enregistrer cette version modifiée de ["^filename^"] ?") ~entry_text:filename ()) in
  ecrire_dans_fichier ~chaine:contenu ~adresse:adresse;
  contenu;;

(** {7 zenity --error} *)
let error
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(no_wrap = false)
    () =
  fairezenity ("error "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(no_wrap_ no_wrap)^
		  " ");;
(*	parse_error ~file:nom_fichier_temporaire;;*)

(** {7 zenity --information} *)
let info
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(no_wrap = false)
    () =
  fairezenity ("info "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(no_wrap_ no_wrap)^
		  " ");;
(*	parse_info ~file:nom_fichier_temporaire;;*)

(** {7 zenity --question} *)
let question
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(ok_label = "") ?(cancel_label = "") ?(no_wrap = false)
    () =
  fairezenity ("question "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(ok_label_ ok_label)^(cancel_label_ cancel_label)^(no_wrap_ no_wrap)^
		  " ");;
(*	parse_question ~file:nom_fichier_temporaire;;*)

(** {7 zenity --warning} *)
let warning
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(no_wrap = false)
    () =
  fairezenity ("warning "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(no_wrap_ no_wrap)^
		  " ");;
(*	parse_warning ~file:nom_fichier_temporaire;;*)

(** {7 zenity --notification} *)
let notification
    ?(title = "") ?(window_icon = "") ?(width = 0) ?(height = 0) ?(expiration = 0)
    ?(text = "") ?(listen = false)
    () =
  fairezenity ("notification "^(title_ title)^(window_icon_ window_icon)^(width_ width)^(height_ height)^(expiration_ expiration)^
		  (text_ text)^(listen_ listen)^
		  " ");
  parse_notification ~file:nom_fichier_temporaire;;


(** {6 Dernières fonctions} *)

(** {7 Liaisons avec l'outil notify-send aussi} *)
type urgency_level = Low | Normal | Critical;;
let print_urgency_level = function
  | Low -> "Low" | Normal -> "Normal" | Critical -> "Critical";;

let notify ?(urgency = Normal) ?(expiration = 1000) ?(icon = "") ?(body = "") ?(summary = "") () =
  faireZ ("notify-send --urgency="^(print_urgency_level urgency)^" --expire-time="^(string_of_int expiration)^" --icon="^(icon)^" \""^summary^"\" \""^body^"\"");;
(** Affiche une notification dans la zone prévue à cet effet (plus performant que 'notification'). *)


(** {7 Fonction d'aide}*)
let help = function
  | "" -> fairezenity ("help"); parse_entry ~file:nom_fichier_temporaire;
  | s  -> fairezenity ("help-\""^s^"\""); parse_entry ~file:nom_fichier_temporaire;;

(* END *)
